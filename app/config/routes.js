import React from 'react';
import {Easing,
Animated} from 'react-native';
import { Icon } from 'react-native-material-ui';
import { StackNavigator, DrawerNavigator,SwitchNavigator } from 'react-navigation';
import MiCuenta from '../screens/MiCuenta';
import Restaurants from '../screens/Restaurants';
import Acerca from '../screens/Acerca';
import DrawerMenu from '../components/Drawer';
import Registro from '../screens/Registro';
import Mapa from '../screens/Mapa';
import Splash from '../screens/Splash';
import { Toolbar } from 'react-native-material-ui';
import RestaurantsDetail from '../screens/RestaurantsDetail';


const restaurantStack = StackNavigator(
  {
    restaurant: {
      screen: Restaurants,
    },
    restaurantDetail: {
      screen: RestaurantsDetail,
    }
  },
  {
    navigationOptions: ({ navigation }) => ({
      initialRouteName: 'SecondScreen',
      headerMode: 'screen',
      headerTitle: `Restaurants`,
      drawerLabel: `Restaurants`,
      header: <Toolbar
        leftElement="menu"
        centerElement="Searchable"
        searchable={{
          autoFocus: true,
          placeholder: 'Search',
        }}
        rightElement={{
            menu: {
                icon: "more-vert",
                labels: ["item 1", "item 2"]
            }
        }}
        onLeftElementPress={() => {
          navigation.navigate('DrawerOpen');
        }}
        onRightElementPress={ (label) => { 
          console.warn("hello")
        }}
      />
    }),
    transitionConfig: () => {
      return {
        transitionSpec: {
          duration: 750,
          easing: Easing.out(Easing.poly(4)),
          timing: Animated.timing,
          useNativeDriver: true,
        },
        screenInterpolator: sceneProps => {      
          const { layout, position, scene } = sceneProps
    
          const thisSceneIndex = scene.index
          const width = layout.initWidth
    
          const translateX = position.interpolate({
            inputRange: [thisSceneIndex - 1, thisSceneIndex],
            outputRange: [width, 0],
          })
    
          return { transform: [ { translateX } ] }
        },
      }
    }
  }
);

const restaurantMenuStack = StackNavigator(
  {
    'restaurant-menu': {
      screen: Acerca,
    }
  },
  {
    navigationOptions: ({ navigation }) => ({
      initialRouteName: 'SecondScreen',
      headerMode: 'screen',
      headerTitle:  `Acerca de Nosotros`,
      drawerLabel: `Acerca de Nosotros`,
      header: <Toolbar
        leftElement="menu"
        centerElement="Acerca de Nosotros"
        searchable={{
          autoFocus: true,
          placeholder: 'Search',
        }}
        rightElement={{
            menu: {
                icon: "more-vert",
                labels: ["item 1", "item 2"]
            }
        }}
        onLeftElementPress={() => {
          navigation.navigate('DrawerOpen');
        }}
        onRightElementPress={ (label) => { 
          console.warn("hello")
        }}
      />
    }),
  }
);

const MapaStack = StackNavigator(
  {
    map: {
      screen: Mapa,
      title: 'Mapa'
    }
  },
  {
    navigationOptions: ({ navigation }) => ({
      initialRouteName: 'SecondScreen',
      headerMode: 'screen',
      headerTitle: 'Mapa',
      drawerLabel: 'Mapa',
      header: <Toolbar
        leftElement="menu"
        centerElement="Mapa"
        searchable={{
          autoFocus: true,
          placeholder: 'Search',
        }}
        rightElement={{
            menu: {
                icon: "more-vert",
                labels: ["item 1", "item 2"]
            }
        }}
        onLeftElementPress={() => {
          navigation.navigate('DrawerOpen');
        }}
        onRightElementPress={ (label) => { 
          console.warn("hello")
        }}
      />
    }),
  }
);


const MainLayout = DrawerNavigator(    //(RouteConfigs, DrawerNavigatorConfig)
  {  
    restaurant: {
      screen: restaurantStack
    },
    'restaurant-menu': {
      screen: restaurantMenuStack
    },
    map: {
      screen: MapaStack,
      navigationOptions: ({ navigation }) => ({
        title: `Mapa`,
      }),
    }
  },
  {                                   //DrawerNavigatorConfig -> 
    initialRouteName: 'restaurant',
    mode: 'modal',
    headerMode: 'none',
    drawerBackgroundColor: '#2EC7AE',
    contentComponent: DrawerMenu
  },
)

const AuthNavigator = StackNavigator(    //(RouteConfigs, DrawerNavigatorConfig)
  {  
    person: MiCuenta,
    SignUp:  Registro
  },{
    headerMode: 'none',
  }
)

export default SwitchNavigator({
  // You could add another route here for authentication.
  // Read more at https://reactnavigation.org/docs/en/auth-flow.html
  Main: MainLayout,
  Auth: AuthNavigator,
  AuthLoading: Splash,
},
  {
    initialRouteName: 'AuthLoading',
  }
);

