import React from 'react';
import { DrawerItems, SafeAreaView } from 'react-navigation';
import { StyleSheet, ScrollView, View, Text, Image, TouchableOpacity } from 'react-native';
import { Divider, Avatar, Drawer, Icon } from 'react-native-material-ui';
import firebase from 'react-native-firebase';

const _onPressButton = () => {
  firebase.auth().signOut();
}

const DrawerMenu = (props) => (
  <View style={styles.container}>
    <View>
      <Drawer.Header  
        style={{
          contentContainer: {
            backgroundColor: "#FFF",
          },
        }}
        // image={<Image source={require('../imgs/drawerBackground.jpg')} />}
        backgroundColor="rgba(255,255,255,0.7)"
      >
        <Drawer.Header.Account
          avatar={<Avatar text="A" />}
          accounts={[
            { avatar: <Avatar text="B" /> },
          ]}
          footer={{
            dense: true,
            centerElement: {
              primaryText: firebase.auth().currentUser.displayName,
              secondaryText: firebase.auth().currentUser.email,
            },
            rightElement: {
              menu: {
                labels: [
                  "1"
                ]
              }
            },
          }}
        />
      </Drawer.Header>

    </View>
    <Divider />
    <View style={styles.container}>
      <ScrollView>
        <SafeAreaView style={styles.container} forceInset={{ top: 'always', horizontal: 'never' }}>
          <DrawerItems {...props}
            getLabel={(scene) => (
              <View style={styles.navItemStyle}>
                <Icon name={scene.route.key} size={26} style={styles.navItemIconStyle} />
                <Text style={styles.navItemTextStyle}>
                  {props.getLabel(scene)}
                  {console.log('AQUIII ' + scene.route.key)}
                </Text>
              </View>
            )}
          />
        </SafeAreaView>
      </ScrollView>
      <View style={styles.footerContainer}>
        <TouchableOpacity onPress={_onPressButton}>
          <Text >Cerrar Sesion</Text>
        </TouchableOpacity>
      </View>
    </View>

  </View>

);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',

  },
  avatar: {
    width: 100,
    height: 100,
    borderRadius: 100 / 2,
    backgroundColor: '#000',
    alignItems: 'center',
    justifyContent: 'center',
  },
  navItemStyle: {
    margin: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  navItemIconStyle: {
    padding: 4
  },
  navItemTextStyle: {
    padding: 5
  },
  footerContainer: {
    padding: 20,
    backgroundColor: 'lightgrey',
    alignItems: 'baseline',
    bottom: 0
  }

});

export default DrawerMenu;