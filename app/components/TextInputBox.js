import React, { Component } from 'react';
import { StyleSheet, View, Text, TextInput } from 'react-native';

export default class TextInputBox extends Component {

  render() {

    const { text='', ...rest } =  this.props;
    return (
      <View style={{
        flexDirection: 'column',
        width: '85%',
        marginBottom: 20
      }}>
        <View style={{
          width: '100%', 
          alignItems: 'center',
          justifyContent: 'center',
          padding: 10,
          borderWidth: 1,
          borderColor: 'white',
          borderStyle: 'dashed',
          borderTopRightRadius: 20,
          borderTopLeftRadius: 20, 
          borderBottomWidth: 0
        }}>
          <Text
            style={{
              fontSize: 18,
              color: 'white',
              fontStyle: 'italic',
            }}
          >{this.props.text}</Text>
        </View>

        <View style={{
          width: '100%', 
          alignItems: 'center',
          justifyContent: 'center',
          padding: 5,
          borderSize: 1,
          borderWidth: 1,
          borderColor: 'white',
          borderStyle: 'dashed',
          borderBottomRightRadius: 20,
          borderBottomLeftRadius: 20
        }}>
          <TextInput
            style={{
              height: 35,
              fontSize: 15,
              width:'86%',
              alignItems: 'center',
              color: 'white',
              justifyContent: 'center'}}
            {...rest}
          />
        </View>
      </View>
    );
  }
}

