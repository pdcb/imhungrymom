import React from 'react';
import Navigator from './config/routes';
import { Provider } from 'react-redux'
import configureStore from './config/configureStore'
import { PersistGate } from 'redux-persist/integration/react'

const store = configureStore()

export default () => (
  <Provider store={store.store}>
    <PersistGate loading={null} persistor={store.persistor}>
      <Navigator onNavigationStateChange={null} />
    </PersistGate>
  </Provider>
  
);