import { ADD_CURRENCY, DELETE_CURRENCY } from '../constants';

const initialState = {
  currencies: [{
    name: 'PEN',
    main: true,
    value: 1,
    label: 'Soles'
  }, {
    name: 'USD',
    main: false,
    value: 0.30581039,
    label: 'Dolares'
  }]
}

export default function peopleReducer(state = initialState, action) {
  switch (action.type) {
    case ADD_CURRENCY:
      return {
        currencies: [...state.currencies, action.currency],
      };
    case DELETE_CURRENCY:
      return {
        currencies: state.people.filter(p => p.name !== action.currency.name),
      };
    default:
      return state;
  }
}
