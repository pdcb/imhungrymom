import React, { Component } from 'react';
import { Text, View, ScrollView, FlatList , ImageBackground,
  StatusBar,
  TouchableOpacity
} from 'react-native';
import { COLOR } from 'react-native-material-ui';

export default class Restaurants extends Component {

  constructor() {
    super();
    this.state = {
      value: '10'
    };
  }

  onChange = (text) => {
    this.setState({ value: text })
  }

  onPress = () => {
    this.props.navigation.navigate('restaurantDetail')
  }
  

  render() {
    return (
      <View style={{
        flex: 1,
        backgroundColor: '#5E2E8C',
      }}
      >
        <StatusBar
          barStyle="light-content"
          backgroundColor={COLOR.blue700}
        />
        <FlatList
          style={{
            marginTop: 15
          }}
          data={[
            {
              key: 'Pizza',
              image: require('../imgs/pizza.png')
            },
            {
              key: 'Ensaladas',
              image: require('../imgs/salad.png')
            },
            {
              key: 'Postres',
              image: require('../imgs/dessert.png')
            },
            {
              key: 'Pasta',
              image: require('../imgs/pasta.png')
            },
            {
              key: 'Bebidas',
              image: require('../imgs/beberage.png')
            }
          ]}
          renderItem={({ item }) => <TouchableOpacity
            style={{
              width:'70%',
              height: 70,
              backgroundColor:'#fff',
              alignSelf: 'center',
              borderRadius: 15,
              marginBottom: 15,
              paddingLeft: 75
            }}
            onPress={this.onPress}
          >
              <ImageBackground
                source={item.image}
                style={{
                  width:70,
                  height:70,
                  position:'absolute',
                  left: -34,
                  top:-3,
                  borderRadius: 35
                }}
                resizeMode='contain'
              />
              <Text style={{
                fontSize: 25,
                fontWeight:'600'
              }}>{item.key}</Text>
              <Text>25 Calorias</Text>
            </TouchableOpacity>
          }
        />
      </View>
    );
  }
}