import React, { Component } from 'react';
import { Text, View } from 'react-native';

export default class Acerca extends Component {

  constructor() {
    super();
    this.state = {
      value: '10'
    };
  }

  onChange = (text) => {
    this.setState({ value: text })
  }

  render() {
    return (
      <View style={{
        flex: 1,
        backgroundColor: '#009E54',
        alignItems: 'center',
        justifyContent: 'center'
      }}>
        <Text
          style={{
            fontSize: 45,
            color: 'white'
          }}
        >Acerca</Text>
      </View>
    );
  }
}