import React, { Component } from 'react';
import { Text, View, PermissionsAndroid ,TextInput, Alert } from 'react-native';
import { ActionButton } from 'react-native-material-ui';
import TextInputBox from '../components/TextInputBox';
import firebase from 'react-native-firebase'
import MapView from 'react-native-maps';

export default class Mapa extends Component {
  static navigationOptions = {
    title: 'Details'
  };

  constructor(props) {
    super(props);
    this.state = {
      userName: '',
      password: ''
    }
  }

  gotoSignUp = () => {
    this.props.navigation.navigate('SignUp')
  }

  goToLocation = () => {
    navigator.geolocation.getCurrentPosition((geo_success) => {
      console.log(geo_success)
    },geo_error => {
      console.log(geo_error)
    });
  }

  render() {
    return (
      <View style={{
        flex: 1,
        backgroundColor: '#FF5608',
        alignItems: 'center',
      }}>
        <MapView
          style={{
            width: '100%',
            height: '100%'
          }}
          initialRegion={{
            latitude: -16.390561,
            longitude: -71.5544056,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          }}
        />
        <ActionButton 
          style={{
            container: {
              backgroundColor: "#1294F6"
            }
          }}
          onPress={this.goToLocation}
          icon="location-searching" 
        /> 
      </View>
    );
  }
}