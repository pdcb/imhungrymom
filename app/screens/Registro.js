import React, { Component } from 'react';
import { Text, View, Alert, ImageBackground } from 'react-native';
import TextInputBox from '../components/TextInputBox';
import firebase from 'react-native-firebase'
import { Button, Divider } from 'react-native-material-ui';
import backgroundImg from '../imgs/dishesBlack.jpg';

const LogIn = ({ children }) => (
  <ImageBackground source={backgroundImg} style={{ flex: 1 }}>
    {children}
  </ImageBackground>
);

const LogInWall = ({ children }) => (
  <ImageBackground style={{ flex: 1, backgroundColor:'black', opacity: 0.7 }}>
    {children}
  </ImageBackground>
);

export default class Registro extends Component {

  constructor() {
    super();
    this.state = {
      value: '',
      userName: '',
      password: ''
    };
  }

  passwordRepeat() {
   this.state.rp = (this.state.password != this.state.passwordR ? "No coincide" : "Correcto");

  }
  onChange = (text) => {
    this.setState({ value: text })
  }

  signUp = () => {
    //this.passwordRepeat();
    firebase.auth().createUserWithEmailAndPassword(this.state.userName, this.state.password)
      .then((user) => {
        console.log(user);
      })
      .catch((er) => {
        Alert.alert(
          'Error',
          er.message,
          [
            { text: 'Ask me later', onPress: () => console.log('Ask me later pressed') },
            { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
            { text: 'OK', onPress: () => console.log('OK Pressed') },
          ],
          { cancelable: false }
        )
      });
  }

  render() {
    return (
      <LogIn>
        <LogInWall>
          <Text style={{
            fontSize: 25,
            color: 'white',
            margin: 20,
            padding: 10,
            textAlign: 'center',
            
          }}>Regístrate</Text>
          <View style={{alignItems: 'center',justifyContent: 'center', margin:20, padding: 10, textAlign:'center'}}>
            <TextInputBox
              text="Usuario"
              onChangeText={(userName) => this.setState({ userName })}
              value={this.state.userName}
            >
            </TextInputBox>
            <TextInputBox
              text="Contraseña"
              onChangeText={(password) => this.setState({ password })}
              value={this.state.password}
              secureTextEntry
            >
            </TextInputBox>
            <TextInputBox
              text="Repetir Contraseña"
              onChangeText={(passwordR) => this.setState({ passwordR })}
              value={this.state.passwordR}
              secureTextEntry
            >
           
            </TextInputBox>
            <Text>{this.state.rp}</Text>
            <Button
              primary
              text="Registrate"
              onPress={this.signUp}
            />
          </View>
          
        </LogInWall>
      </LogIn>
    );
  }
}