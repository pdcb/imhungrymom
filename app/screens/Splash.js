import React from 'react';
import { Dimensions, ImageBackground, Text } from 'react-native';
import * as Animatable from 'react-native-animatable';
import bgSplash from '../imgs/bgSplash.png';
import firebase from 'react-native-firebase';
import move1 from '../imgs/move1.png';
import move2 from '../imgs/move2.png';
import move3 from '../imgs/move3.png';

const MONEY_DIMENSIONS = { width: 30, height: 10 };
const SCREEN_DIMENSIONS = Dimensions.get('window');
const WIGGLE_ROOM = 170;

const FlippingImage = ({ back = false, delay, duration = 1000, source, style = {} }) => (
  <Animatable.Image
    animation={{
      from: { rotate: !back ? '180deg' : '0deg' },
      to: { rotate: !back ? '180deg' : '0deg' },
    }}
    duration={duration}
    delay={delay}
    easing="linear"
    iterationCount="infinite"
    useNativeDriver
    source={source}
    style={{
      ...style,
      backfaceVisibility: 'hidden',
    }}
  />
);


const Swinging = ({ amplitude, rotation = 7, delay, duration = 700, children, left = 0, top = 0 }) => (
  <Animatable.View
    animation={{
      0: {
        translateX: -amplitude,
        translateY: -amplitude * 0.8,
        rotate: `${rotation}deg`,
      },
      0.4: {
        translateX: 0,
        translateY: 0,
        rotate: '0deg',
      },
      1: {
        translateX: amplitude,
        translateY: -amplitude * 0.8,
        rotate: `${-rotation}deg`,
      },
    }}
    delay={delay}
    duration={duration}
    direction="alternate"
    easing="ease-in-out"
    iterationCount="infinite"
    useNativeDriver
    style={{
      left: left,
      top: top
    }}
  >
    {children}
  </Animatable.View>
);

const Falling = ({ duration, delay, style, children }) => (
  <Animatable.View
    animation={{
      from: { translateY: SCREEN_DIMENSIONS.height - 360 },
      to: { translateY: SCREEN_DIMENSIONS.height - 360 },
    }}
    duration={duration}
    delay={delay}
    easing={t => Math.pow(t, 1.7)}
    iterationCount="infinite"
    useNativeDriver
    style={{
      paddingHorizontal: WIGGLE_ROOM,
      //left: randomize(SCREEN_DIMENSIONS.width) - WIGGLE_ROOM,
    }}
  >
    {children}
  </Animatable.View>
);

const Table = ({ children }) => (
  <ImageBackground source={bgSplash} style={{ flex: 1 }}>
    {children}
  </ImageBackground>
);

const randomize = max => Math.random() * max;

const range = count => {
  const array = [];
  for (let i = 0; i < count; i++) {
    array.push(i);
  }
  return array;
};

export default class Splash extends React.Component {
  constructor(props) {
    super(props);
    this._bootstrapAsync();
  }
  
  // Fetch the token from storage then navigate to our appropriate place
  _bootstrapAsync = () => {
    setTimeout(() => {
      firebase.auth().onAuthStateChanged(user => {
        this.props.navigation.navigate(user ? 'Main' : 'Auth')
      }) 
    }, 1500)
    
    /* const userToken = await AsyncStorage.getItem('userToken');

    // This will switch to the App screen or Auth screen and this loading
    // screen will be unmounted and thrown away.
    this.props.navigation.navigate(userToken ? 'Main' : 'Auth'); */
  };

  render() {
    const count = 1;
    const duration = 0;
    return (

      <Table>
        <Animatable.Text 
          animation="swing"
          delay={500}
        style={{
          fontSize: 35,
          textAlign: 'center',
          fontFamily: 'Elephant',
          color: 'white',
          padding: 30
        }}>I'M HUNGRY MOM</Animatable.Text>
        {range(count)
          .map(i => randomize(1000))
          .map((flipDelay, i) => (
            <Falling
              key={i}
              duration={duration}
              delay={i * (duration / count)}
              style={{
                position: 'absolute',
                paddingHorizontal: WIGGLE_ROOM,
                left: randomize(SCREEN_DIMENSIONS.width - MONEY_DIMENSIONS.width) - WIGGLE_ROOM,
              }}
            >
              <Swinging amplitude={MONEY_DIMENSIONS.width / 5}
                delay={randomize(duration)}
              >
                <FlippingImage source={move1} delay={flipDelay} />
              </Swinging>
              <Swinging amplitude={MONEY_DIMENSIONS.width / 5}
                delay={randomize(duration)}
                left={55} top={-45}
              >
                <FlippingImage source={move2} delay={flipDelay} />
              </Swinging>
              <Swinging amplitude={MONEY_DIMENSIONS.width / 5}
                delay={randomize(duration)}
                left={110} top={-65}
              >
                <FlippingImage source={move3} delay={flipDelay} />
              </Swinging>
            </Falling>
          ))}
      </Table>
    )
  }
}


