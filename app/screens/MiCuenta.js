import React, { Component } from 'react';
import {
  Text, View, TextInput,
  Alert, Image, ImageBackground, ScrollView,
  KeyboardAvoidingView, Platform, Dimensions
} from 'react-native';
import { Button } from 'react-native-material-ui';
import TextInputBox from '../components/TextInputBox';
import firebase from 'react-native-firebase'
import backgroundImg from '../imgs/vegetables.jpg';
import { Header } from 'react-navigation';
const { width, height } = Dimensions.get('window')
const imageStyle = {
  width: width < height ? width : height,
  height: width < height ? height : width,
  resizeMode: 'stretch',
}

const LogIn = ({ children }) => (
  children
);

const LogInWall = ({ children }) => (
  <View
    style={{ 
      flex: 1
  }}
  // behavior="padding"
  >
    <ImageBackground
      source={backgroundImg}
      style={{
        flex: 1,
        width: width,
        height: height
      }}
    >
      <ScrollView
        style={{
          backgroundColor:'black',
          opacity: 0.7 
        }}
       
      >
        {children}
      </ScrollView>


    </ImageBackground>

  </View>
);

export default class MiCuenta extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: '',
      password: ''
    }
  }

  gotoSignUp = () => {
    this.props.navigation.navigate('SignUp')
  }

  login = () => {

    firebase.auth().signInWithEmailAndPassword(this.state.userName, this.state.password)
      .then((user) => {
        console.log(user);
      })
      .catch((er) => {
        Alert.alert(
          'Error',
          er.message,
          [
            { text: 'Ask me later', onPress: () => console.log('Ask me later pressed') },
            { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
            { text: 'OK', onPress: () => console.log('OK Pressed') },
          ],
          { cancelable: false }
        )
      });
  }




  render() {
    return (
      <LogIn>
        <LogInWall>
          <Text style={{
            fontSize: 25,
            color: 'white',
            margin: 20,
            padding: 10,
            textAlign: 'center',

          }}>Iniciar Sesion</Text>

          <View style={{ alignItems: 'center', justifyContent: 'center', margin: 20, padding: 10, textAlign: 'center' }}>
            <TextInputBox
              text="Usuario"
              onChangeText={(userName) => this.setState({ userName })}
              value={this.state.userName}

            >
            </TextInputBox>
            <TextInputBox
              text="Contraseña"
              onChangeText={(password) => this.setState({ password })}
              value={this.state.password}
              secureTextEntry
            >
            </TextInputBox>
          </View>
          <Button
            primary
            text="ENTRAR"
            onPress={this.login}
          />
          <Button
            primary
            text="Registrate"
            onPress={this.gotoSignUp}
          />
        </LogInWall>
      </LogIn>
    );
  }
}