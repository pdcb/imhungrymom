import React, { Component } from 'react';
import { Text, View, ScrollView, StatusBar, FlatList , ImageBackground} from 'react-native';
import { COLOR,Toolbar } from 'react-native-material-ui';

export default class RestaurantsDetail extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: `I am screen 1`,
      headerStyle: {
        backgroundColor: '#f39c12',
        height: 85,
      },
      headerTitleStyle: {
        color: 'white'
      },
      header: <Toolbar
        leftElement="menu"
        centerElement="Searchable"
        searchable={{
          autoFocus: true,
          placeholder: 'Search',
        }}
        style={{
          container: {
            backgroundColor:COLOR.red500
          }
        }}
        rightElement={{
            menu: {
                icon: "more-vert",
                labels: ["item 1", "item 2"]
            }
        }}
        onLeftElementPress={() => {
          navigation.navigate('DrawerOpen');
        }}
        onRightElementPress={ (label) => { 
          console.warn("hello")
        }}
      />
    }
  }

  constructor() {
    super();
    this.state = {
      value: '10'
    };
  }

  onChange = (text) => {
    this.setState({ value: text })
  }

  render() {
    return (
      <View style={{
        flex: 1,
        backgroundColor: '#5E2E8C',
      }}
      >
        <StatusBar
          barStyle="light-content"
          backgroundColor={COLOR.red700}
        />
        <FlatList
          style={{
            marginTop: 15
          }}
          data={[
            {
              key: 'Pizza',
              image: require('../imgs/pizza.png')
            },
            {
              key: 'Ensaladas',
              image: require('../imgs/salad.png')
            },
            {
              key: 'Postres',
              image: require('../imgs/dessert.png')
            },
            {
              key: 'Pasta',
              image: require('../imgs/pasta.png')
            },
            {
              key: 'Bebidas',
              image: require('../imgs/beberage.png')
            }
          ]}
          renderItem={({ item }) => <View
            style={{
              width:'70%',
              height: 70,
              backgroundColor:'#fff',
              alignSelf: 'center',
              borderRadius: 15,
              marginBottom: 15,
              paddingLeft: 75
            }}
          >
              <ImageBackground
                source={item.image}
                style={{
                  width:70,
                  height:70,
                  position:'absolute',
                  left: -34,
                  top:-3,
                  borderRadius: 35
                }}
                resizeMode='contain'
              />
              <Text style={{
                fontSize: 25,
                fontWeight:'600'
              }}>{item.key}</Text>
              <Text>25 Calorias</Text>
            </View>
          }
        />
      </View>
    );
  }
}